# Ticket web application

The application consists of an HTTP API and application logic.

# API

The API works in JSON format and consists of two services:

1)Service for accepting request for payment. 
Receive, validate and save the request in the database.
At the entrance accepts:Route number, Date and time of departure.
The output gives (if successful):Request ID.

2)Service for checking the status of request. 
Gets data about the status of request from the database.
At the entrance accepts:Request ID.
The output gives:Request Status.

Application logic:

The process of payment (runs once a minute):

Selects a request suitable for holding from the database;
Sends a request to the http-service 
(the service implemented in the same application as a separate endpoint) 
of the payment gateway, which randomly returns statuses 
(processing, error, done).
Updates request status.

### Preconditions

Install lombok plugin

Install MySQL Server

In resources folder change application.properties:
- spring.datasource.username=...
- spring.datasource.password=...

Application is ready for use

In Postman check work of PaymentRequestController:

1)POST  localhost:8075/api/v1/paymentrequest/
Request:
{
	"routeId":1,
	"departureTime":"2020-10-20T12:12:12"
}

Response: 10

2)GET localhost:8075/api/v1/paymentrequest/10

Response: "DONE"
