package com.dyriavko.ticketpayment.repository;

import com.dyriavko.ticketpayment.model.Route;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RouteRepository extends JpaRepository<Route, Long> {
    Route findByRoute(String route);
}
