package com.dyriavko.ticketpayment.repository;

import com.dyriavko.ticketpayment.model.PaymentRequest;
import com.dyriavko.ticketpayment.model.RequestStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface PaymentRequestRepository extends JpaRepository<PaymentRequest, Long> {

    @Query("select r from PaymentRequest r where r.requestStatus = :status")
    List<PaymentRequest> findProcessingRequests(@Param("status") RequestStatus status);
}
