package com.dyriavko.ticketpayment.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class PaymentRequestDto implements Serializable {
    private Long routeId;
    private LocalDateTime departureTime;
}
