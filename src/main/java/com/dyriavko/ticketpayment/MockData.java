package com.dyriavko.ticketpayment;

import com.dyriavko.ticketpayment.model.PaymentRequest;
import com.dyriavko.ticketpayment.model.RequestStatus;
import com.dyriavko.ticketpayment.model.Route;
import com.dyriavko.ticketpayment.repository.PaymentRequestRepository;
import com.dyriavko.ticketpayment.repository.RouteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import java.time.LocalDateTime;
import java.util.Objects;
import javax.annotation.PostConstruct;

@Configuration
public class MockData {

    public static final String ROUTE_1 = "Route 1";
    @Autowired
    private PaymentRequestRepository paymentRequestRepository;
    @Autowired
    private RouteRepository routeRepository;


    @PostConstruct
    public void populateDatabase() {
        Route route = createRoute();
        createRequest(route, 1);
        createRequest(route, 2);
        createRequest(route, 3);
    }

    private void createRequest(Route route, int i) {
        PaymentRequest request = new PaymentRequest();
        request.setRequestStatus(RequestStatus.PROCESSING);
        request.setRoute(route);
        request.setDepartureTime(LocalDateTime.now().plusDays(i));
        paymentRequestRepository.save(request);
    }

    private Route createRoute() {
        if (Objects.isNull(routeRepository.findByRoute(ROUTE_1))) {
            Route route = new Route();
            route.setRoute(ROUTE_1);
            return routeRepository.save(route);
        }
        return routeRepository.findByRoute(ROUTE_1);
    }


}