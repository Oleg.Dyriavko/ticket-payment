package com.dyriavko.ticketpayment.rest;

import com.dyriavko.ticketpayment.service.PaymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api/v1")
public class PaymentController {

    @Autowired
    private PaymentService paymentService;

    @GetMapping(value = "/payment")
    public ResponseEntity<String> getRandomStatus() {
        return new ResponseEntity<>(paymentService.getRandomStatus().name(), HttpStatus.OK);
    }
}
