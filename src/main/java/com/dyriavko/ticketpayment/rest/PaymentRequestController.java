package com.dyriavko.ticketpayment.rest;

import com.dyriavko.ticketpayment.dto.ErrorDto;
import com.dyriavko.ticketpayment.dto.PaymentRequestDto;
import com.dyriavko.ticketpayment.facade.PaymentRequestFacade;
import com.dyriavko.ticketpayment.model.RequestStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api/v1/paymentrequest")
public class PaymentRequestController {

    @Autowired
    private PaymentRequestFacade paymentRequestFacade;

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<RequestStatus> getRequestById(@PathVariable(name = "id") Long id) {
        return new ResponseEntity<>(paymentRequestFacade.findById(id), HttpStatus.OK);
    }


    @RequestMapping(value = "/", method = RequestMethod.POST)
    public ResponseEntity<Long> createRequest(@RequestBody PaymentRequestDto dto) {
        return new ResponseEntity<>(paymentRequestFacade.createPaymentRequest(dto), HttpStatus.OK);
    }

    @ExceptionHandler(RuntimeException.class)
    public ResponseEntity<ErrorDto> entityNotFoundHandler(RuntimeException e) {
        return new ResponseEntity<>(new ErrorDto(e.getMessage()), HttpStatus.BAD_REQUEST);
    }

}


