package com.dyriavko.ticketpayment;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableScheduling
@SpringBootApplication
public class TicketPaymentApplication {

    public static void main(String[] args) {
        SpringApplication.run(TicketPaymentApplication.class, args);
    }

}
