package com.dyriavko.ticketpayment.model;

import java.util.NoSuchElementException;

public enum RequestStatus {
    ERROR("error"),
    PROCESSING("processing"),
    DONE("done");

    private final String fieldDescription;

    RequestStatus(String name) {
        fieldDescription = name;
    }

    public static RequestStatus of(String name) {
        for (RequestStatus requestStatus : RequestStatus.values()) {
            if (requestStatus.fieldDescription.equalsIgnoreCase(name)) {
                return requestStatus;
            }
        }
        throw new NoSuchElementException("RequestStatus with name " + name + " not found");
    }
}
