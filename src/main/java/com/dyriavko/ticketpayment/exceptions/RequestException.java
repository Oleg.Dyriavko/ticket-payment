package com.dyriavko.ticketpayment.exceptions;

public class RequestException extends RuntimeException {
    public RequestException(String message) {
        super(message);
    }
    public RequestException(){
        super();
    }
}
