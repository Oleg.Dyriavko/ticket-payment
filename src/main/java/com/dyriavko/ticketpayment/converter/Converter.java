package com.dyriavko.ticketpayment.converter;


public interface Converter<S, T> extends org.springframework.core.convert.converter.Converter<S, T>{
    T convert(S s);
}
