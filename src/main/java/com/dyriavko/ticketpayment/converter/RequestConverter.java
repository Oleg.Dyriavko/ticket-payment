package com.dyriavko.ticketpayment.converter;

import com.dyriavko.ticketpayment.dto.PaymentRequestDto;
import com.dyriavko.ticketpayment.model.PaymentRequest;
import com.dyriavko.ticketpayment.model.Route;
import com.dyriavko.ticketpayment.repository.RouteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.EntityNotFoundException;

@Component
public class RequestConverter implements Converter<PaymentRequestDto, PaymentRequest> {

    @Autowired
    private RouteRepository routeRepository;

    @Override
    public PaymentRequest convert(PaymentRequestDto dto) {
        PaymentRequest paymentRequest = new PaymentRequest();
        paymentRequest.setDepartureTime(dto.getDepartureTime());
        paymentRequest.setRoute(createRoute(dto));
        return paymentRequest;
    }

    private Route createRoute(PaymentRequestDto dto) {
        return routeRepository.findById(dto.getRouteId())
                .orElseThrow(() -> new EntityNotFoundException("Route does not found!"));
    }
}
