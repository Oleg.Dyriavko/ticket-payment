package com.dyriavko.ticketpayment.converter;

import com.dyriavko.ticketpayment.dto.PaymentRequestDto;
import com.dyriavko.ticketpayment.model.PaymentRequest;
import org.springframework.stereotype.Component;

@Component
public class RequestDtoConverter implements Converter<PaymentRequest, PaymentRequestDto> {

    @Override
    public PaymentRequestDto convert(PaymentRequest paymentRequest) {
        PaymentRequestDto paymentRequestDto = new PaymentRequestDto();
        paymentRequestDto.setRouteId(paymentRequest.getRoute().getId());
        paymentRequestDto.setDepartureTime(paymentRequest.getDepartureTime());
        return paymentRequestDto;
    }

}
