package com.dyriavko.ticketpayment.service;

import com.dyriavko.ticketpayment.model.RequestStatus;

public interface PaymentService {

    RequestStatus getRandomStatus();
}
