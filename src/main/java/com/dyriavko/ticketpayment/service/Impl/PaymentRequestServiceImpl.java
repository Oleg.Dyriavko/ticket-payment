package com.dyriavko.ticketpayment.service.Impl;

import com.dyriavko.ticketpayment.exceptions.RequestException;
import com.dyriavko.ticketpayment.model.PaymentRequest;
import com.dyriavko.ticketpayment.model.RequestStatus;
import com.dyriavko.ticketpayment.repository.PaymentRequestRepository;
import com.dyriavko.ticketpayment.service.PaymentRequestService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
@Slf4j
public class PaymentRequestServiceImpl implements PaymentRequestService {

    private final PaymentRequestRepository paymentRequestRepository;

    @Autowired
    public PaymentRequestServiceImpl(PaymentRequestRepository paymentRequestRepository) {
        this.paymentRequestRepository = paymentRequestRepository;
    }


    @Override
    public PaymentRequest create(PaymentRequest paymentRequest) {
        if (paymentRequest.getDepartureTime().isBefore(LocalDateTime.now())) {
            throw new RuntimeException();
        }
        paymentRequest.setRequestStatus(RequestStatus.PROCESSING);
        PaymentRequest request = paymentRequestRepository.save(paymentRequest);
        log.info("IN create - paymentRequest:{} successfully created", request);
        return request;
    }

    @Override
    public PaymentRequest findById(Long id) {
        PaymentRequest paymentRequest = paymentRequestRepository.findById(id).orElseThrow(() -> new RequestException("Request not found"));
        log.info("IN findById - paymentRequest:{} found by Id: {}", paymentRequest, id);
        return paymentRequest;
    }
}
