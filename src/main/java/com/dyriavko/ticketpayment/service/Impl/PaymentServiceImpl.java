package com.dyriavko.ticketpayment.service.Impl;

import com.dyriavko.ticketpayment.model.RequestStatus;
import com.dyriavko.ticketpayment.service.PaymentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Random;

@Service
@Slf4j
public class PaymentServiceImpl implements PaymentService {

    @Override
    public RequestStatus getRandomStatus() {
        RequestStatus[] values = RequestStatus.values();
        RequestStatus requestStatus = values[new Random().nextInt(values.length)];
        log.info("IN getRandomStatus - got requestStatus: {}", requestStatus);
        return requestStatus;
    }
}
