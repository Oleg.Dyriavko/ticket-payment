package com.dyriavko.ticketpayment.service.Impl;

import com.dyriavko.ticketpayment.model.PaymentRequest;
import com.dyriavko.ticketpayment.model.RequestStatus;
import com.dyriavko.ticketpayment.repository.PaymentRequestRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Slf4j
@Service
public class ProcessingRequestService {

    private static final int DELAY = 60000;

    @Autowired
    private PaymentRequestRepository paymentRequestRepository;

    @Scheduled(fixedDelay = DELAY)
    public void execute() {
        log.info("IN execute - start ProcessingRequestService");
        List<PaymentRequest> paymentRequestList = paymentRequestRepository.findProcessingRequests(RequestStatus.PROCESSING);
        log.info("----- '{}' processing requests founded", paymentRequestList.size());
        paymentRequestList.forEach(request -> {
            request.setRequestStatus(paymentCall());
        });
        log.info("IN execute - got list of paymentRequests with requestStatus PROCESSING");
        paymentRequestRepository.saveAll(paymentRequestList);
        log.info("IN execute - end ProcessingRequestService");
    }

    private RequestStatus paymentCall() {
        final String uri = "http://localhost:8075/api/v1/payment";
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<String> response = restTemplate.exchange(uri,
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<String>() {
                });

        String result = response.getBody();
        log.info("IN paymentCall - got requestStatuses is '{}", result);

        return RequestStatus.of(result);
    }
}
