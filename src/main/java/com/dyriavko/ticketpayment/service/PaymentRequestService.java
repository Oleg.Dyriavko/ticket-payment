package com.dyriavko.ticketpayment.service;

import com.dyriavko.ticketpayment.model.PaymentRequest;

public interface PaymentRequestService {

    PaymentRequest findById(Long id);

    PaymentRequest create(PaymentRequest paymentRequest);


}
