package com.dyriavko.ticketpayment.facade;

import com.dyriavko.ticketpayment.converter.Converter;
import com.dyriavko.ticketpayment.dto.PaymentRequestDto;
import com.dyriavko.ticketpayment.model.PaymentRequest;
import com.dyriavko.ticketpayment.model.RequestStatus;
import com.dyriavko.ticketpayment.service.PaymentRequestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PaymentRequestFacadeImpl implements  PaymentRequestFacade {

    @Autowired
    private Converter<PaymentRequestDto, PaymentRequest> converter;

    @Autowired
    private PaymentRequestService paymentRequestService;

    @Override
    public Long createPaymentRequest(PaymentRequestDto dto) {
        PaymentRequest paymentRequest = converter.convert(dto);
        return  paymentRequestService.create(paymentRequest).getId();
    }

    @Override
    public RequestStatus findById(Long id) {
        PaymentRequest paymentRequest = paymentRequestService.findById(id);
        return  paymentRequest.getRequestStatus();
    }
}
