package com.dyriavko.ticketpayment.facade;

import com.dyriavko.ticketpayment.dto.PaymentRequestDto;
import com.dyriavko.ticketpayment.model.RequestStatus;

public interface PaymentRequestFacade {
    Long createPaymentRequest(PaymentRequestDto dto);
    RequestStatus findById(Long id);
}
